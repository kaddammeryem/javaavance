
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;

public class ParcoursThread extends Thread {
	//nom du fichier a rechercher
	private String nameFile;
	// chemin racine de recherche
	private Path chemin;
	// =0 si fichier retrouve et =1 si non
	protected static int trouve;
	//liste des sous-repertoires du repertoire racine
	private ArrayList<Path> lis=new ArrayList<>();

	
	public ParcoursThread (String name,Path chemin) {
		this.nameFile=name;
		this.chemin=chemin;
		trouve=0;
		
		
	}
	public ParcoursThread () {
	
	}
	
	public  int  recherche ( ) {
		try {
			/*Permet de faire une iteration sur les fichiers du chemin 
			passe en argument*/
			DirectoryStream<Path> ds = Files.newDirectoryStream(chemin);
			
		
			Iterator <Path> it=ds.iterator();
			while(it.hasNext()){
				Path p =it.next();
				//on recupere le nom de chaque fichier
				String n=p.getFileName().toString();
			
				/*si le nom du fichier sur lequel on pointe est 
				 egale a celui recherche  */
				
				if(n.equals(nameFile)) {
					
					trouve=1;
					//on affiche le chemin absolu vers ce fichier
					System.out.println("le fichier "+nameFile+" existe , voici son chemin : "
					+p.getParent());
					ds.close();
					return 1;
					
				
					}
				
				}
			ds.close();}
		 catch (IOException e) {
			// TODO Auto-generated catch block
			
			
		}
		return 0;
	}
	public String getNameFile() {
		return nameFile;
	}
	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}
	public Path getChemin() {
		return chemin;
	}
	public void setChemin(Path chemin) {
		this.chemin = chemin;
	}
	
	
	public  void run() {
		
	    	this.recherche();
	       
	 } 
	    
	public ArrayList<Path> listFichier(Path ch) {
		try {
			ArrayList<Path> list=new ArrayList<>();
			DirectoryStream<Path> ds = Files.newDirectoryStream(ch);
		
		/*ici on liste les fichiers d'un repertoire passe en argument , 
	cette methode est concue uniquement pour etre utilisee dans liste repertoire
		 */
			Iterator <Path> it=ds.iterator();
			while(it.hasNext()){
				Path p =it.next();
				list.add(p);
			}
			ds.close();
			return list;
			
			}
			 catch (IOException e) {
					// TODO Auto-generated catch block
					
					
				}
				return null;
	}
	
	
	public  ArrayList<Path> listeRepertoire ( Path rep) {
	
		if ( Files.isDirectory (rep ) ) {
        		
                ArrayList<Path> list = this.listFichier(rep);
                if (list != null){
	                for ( int i = 0; i < list.size(); i++) {
	                        /*Appel récursif sur les sous-répertoires
	                        pour extraire tous les repertoires du dossier racine
	                         */
	                       
	                        
	                	if(Files.isDirectory(list.get(i))){
	                        lis.add(list.get(i));}
	                        listeRepertoire( list.get(i));
	                        
	                    //124 , 136,  129 
	                        //136,123,137
	                } 
	               
	            } 
                else {
                	
                }
               
        } 
        return lis;
        
	
	
	
	
	}
	
}
	