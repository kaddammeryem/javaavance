import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class ParcoursTri extends Thread{
	private Path chemin;
	private ArrayList<Path> lis=new ArrayList<>();

	
	public ParcoursTri(Path chemin) {
		this.chemin=chemin;
	}
	public void tri() {
		try {
			DirectoryStream<Path> ds = Files.newDirectoryStream(chemin);
		
			Iterator <Path> it=ds.iterator();
			Comparator<File> PathNameComparator = Comparator.comparing(java.io.File::length);
			Set <File>t=new TreeSet<>(PathNameComparator);
			
			
			while(it.hasNext()){
				Path p =it.next();
				t.add(p.toFile());
				
				}
				
		    
			Iterator<File> it1=t.iterator();
			int i=0;
			
			while(it1.hasNext() && i<10) {
				
				System.out.println(it1.next());
				i++;}
			
			
		
				ds.close();
			}
			
		 catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		
	}
	public void run() {
		tri();
	}
	public ArrayList<Path> listFichier(Path ch) {
		try {
			ArrayList<Path> lis=new ArrayList<>();
			DirectoryStream<Path> ds = Files.newDirectoryStream(ch);
		
		
			Iterator <Path> it=ds.iterator();
			while(it.hasNext()){
				Path p =it.next();
				lis.add(p);
			}
			ds.close();
			return lis;
			
			}
			 catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					
				}
				return null;
	}
	
	
	public  ArrayList<Path> listeRepertoire ( Path rep) {
       
		 if ( Files.isDirectory (rep ) ) {
        		
                ArrayList<Path> list = this.listFichier(rep);
                if (list != null){
	                for ( int i = 0; i < list.size(); i++) {
	                        // Appel récursif sur les sous-répertoires
	                       
	                        //System.out.println("le fichier est "+list.get(i));
	                	if(Files.isDirectory(list.get(i))){
	                        lis.add(list.get(i));}
	                        listeRepertoire( list.get(i));
	                        
	                       
	                } 
	               } 
                else {
                	System.err.println(rep + " : Erreur de lecture.");
                }
        } 
        return lis; 
        
        
}
	

}
