
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;


public class ParcoursTriThreadMain {
	public static void main(String [ ] args) {
		System.out.println(" +------------------------------------------------------------------------------+");
		System.out.println("+|                  Bienvenue dans votre trieur de fichiers                     |+");
		System.out.println(" +------------------------------------------------------------------------------+");
		Scanner sc=new Scanner(System.in);
		System.out.println("donner le chemin d'un dossier a trier");
		
		String name=sc.next();
		Path chemin=FileSystems.getDefault().getPath(name);
		//  /Users/kaddammeryem/Desktop/ex3
		System.out.println("donner le nombre de thread a utiliser");
		int nbrThread=sc.nextInt();
		
	
		//ThreadGroup monThreadGroup = new ThreadGroup("Mon pool de threads");
		
		ParcoursTri p1=new ParcoursTri(chemin);
		//Thread t1=new Thread(monThreadGroup,p1,"one");  
		
		ArrayList<Path> listPath=p1.listeRepertoire(chemin);
	
		
		int i=0;
	///Users/kaddammeryem/eclipse-workspace/Voiture
	
		String pattern = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		

		System.out.println("début du programme: "+ simpleDateFormat.format(new java.util.Date().getTime()));
	
		while(i<listPath.size() ) {
				
				
					int f=0;
					
					//System.out.println("le nombre "+Thread.activeCount());
				
					while(f<nbrThread && i<listPath.size()) {
						p1.start();
						
					
						chemin=listPath.get(i);
						
						
						i++;
						f++;
						
						
						p1=new ParcoursTri (chemin);
						
						
						}
					}
		
			try {
				p1.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			sc.close();
	
		
		//date fin
		//ici on fait la difference en rpecisant dans l affichage le nbr de thread utilise
			
		
		System.out.println("fin du programme: "+ simpleDateFormat.format(new java.util.Date().getTime()));
	
				
			
					
				
					
			
		
		
		

		

}

}
