
public class TestTrain {
	

	    public static void main (String[] args) {

	    	Train tgv = new Train(10, "TGV");

	    	Train corail = new Train(20,"CORAIL");

	    	Train micheline = new Train (50, "MICHELINE");

	    	micheline.start();
	    	tgv.start();
	    	corail.start();

	    	System.out.println("Threads actifs : " +

	    	Thread.currentThread().activeCount());
	    	
	    	System.out.println("Threads actifs names : " +

	    	Thread.currentThread().getName());

	    	try {

	    	corail.join();
	    	System.out.println("Threads actifs : "+Thread.currentThread().getName());// attendre la fin de corail !

	    	} catch (InterruptedException ex) { }

	    	System.out.println("Threads actifs : " +

	    	Thread.currentThread().activeCount());

	    	System.out.println("fin du main");

	    	}

	    	}



